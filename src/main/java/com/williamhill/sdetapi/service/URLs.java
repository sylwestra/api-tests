package com.williamhill.sdetapi.service;

public class URLs {
    public static final String SESSIONS_API = "https://sandbox.whapi.com/v1/sessions/tickets";
    public static final String COMPETITIONS_IN_PLAY = "https://sandbox.whapi.com/v1/competitions/events/inplay";
}
