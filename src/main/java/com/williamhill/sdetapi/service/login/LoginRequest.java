package com.williamhill.sdetapi.service.login;

import com.jayway.restassured.response.Response;

import static com.jayway.restassured.RestAssured.given;
import static com.williamhill.sdetapi.service.URLs.SESSIONS_API;

public class LoginRequest extends AuthenticateRequest {

    public Response loginCustomerWithoutUsername() {
        return given().
                spec(getDefaultHeaderForXMl().build()).
                param("password", user.get("password")).
                param("extended", "Y").
                post(SESSIONS_API);
    }

    public Response loginCustomerWithoutPassword() {
        return given().
                spec(getDefaultHeaderForXMl().build()).
                param("username", user.get("username")).
                param("extended", "Y").
                post(SESSIONS_API);
    }


}
