package com.williamhill.sdetapi.service.login;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;
import static com.williamhill.sdetapi.service.Params.EXTENDED;
import static com.williamhill.sdetapi.service.Params.PASSWORD;
import static com.williamhill.sdetapi.service.Params.USERNAME;
import static com.williamhill.sdetapi.service.URLs.*;

public class AuthenticateRequest {

    protected static final Map<String, String> userPass = new HashMap<String, String>(){
        {
            put(USERNAME, "QASDET1");
            put(PASSWORD, "QAAPI1");
        }
    };

    public Response loginCustomer() {
        return given().
                spec(getDefaultHeaderForXMl().build()).
                param(USERNAME, userPass.get(USERNAME)).
                param(PASSWORD, userPass.get(PASSWORD)).
                param(EXTENDED, "Y").
                post(SESSIONS_API);
    }

    public Response authenticate(String ticket) {
        return given().
                spec(getDefaultHeaderForXMlApi(ticket).build()).
                param(USERNAME, userPass.get(USERNAME)).
                param(PASSWORD, userPass.get(PASSWORD)).
                get(ACCOUNTS_API);
    }

    public Response authenticateBalance(String ticket) {
        return given().
                spec(getDefaultHeaderForXMlApi(ticket).build()).
                param(USERNAME, userPass.get(USERNAME)).
                param(PASSWORD, userPass.get(PASSWORD)).
                get(ACCOUNTS_BALANCE);
    }
    
    public Response authenticatePrices() {
        return given().
                spec(getDefaultHeaderForXMlApiPrices().build()).
                param("eventsToday", "Y").
                get(COMPETITIONS_IN_PLAY);
    }

    public Response authenticateBet(String ticket) {
        return given().
                spec(getDefaultHeaderForXMlApi2(ticket).build()).
                param("legType", "W").
                param("stake", "1.00").
                param("outcomeId", "32520777").
                param("priceType", "L").
                param("priceNum", "1").
                param("priceDen", "1").
                param("betNum", "1").
                post("https://sandbox.whapi.com/v1/bets/me");
    }

    public Response authenticateBetWithoutReuiredParam(String ticket) {
        return given().
                spec(getDefaultHeaderForXMlApi2(ticket).build()).
                param("stake", "1.00").
                param("outcomeId", "32520777").
                param("priceType", "L").
                param("priceNum", "1").
                param("priceDen", "1").
                post("https://sandbox.whapi.com/v1/bets/me");
    }

    public Response getBet(String ticket) {
        return given().
                spec(getDefaultHeaderForXMlApi2(ticket).build()).param("blockSize", 1).param("blockNum", 1).
                get("https://sandbox.whapi.com/v1/bets/me");
    }


    protected static RequestSpecBuilder getDefaultHeaderForXMl() {
        return new RequestSpecBuilder().addHeader("Accept", "application/xml").addHeader("Content-Type", "application/x-www-form-urlencoded").
                addHeader("who-apiKey", "l7xxdbad8c0b183147f99f464f42ded229be").
                addHeader("who-secret", "35ba66acb7b64e2ea8dd895dd27616f5");
    }

    private static RequestSpecBuilder getDefaultHeaderForXMlApi(String ticket) {
        return new RequestSpecBuilder().addHeader("Accept", "application/xml").addHeader("Content-Type", "application/x-www-form-urlencoded").
                addHeader("who-ticket", ticket).
                addHeader("who-apiKey", "l7xxdbad8c0b183147f99f464f42ded229be").
                addHeader("who-secret", "35ba66acb7b64e2ea8dd895dd27616f5");
    }

    private static RequestSpecBuilder getDefaultHeaderForXMlApi2(String ticket) {
        return new RequestSpecBuilder().addHeader("Accept", "application/xml").
                addHeader("who-ticket", ticket).
                addHeader("who-apiKey", "l7xxdbad8c0b183147f99f464f42ded229be").
                addHeader("who-secret", "35ba66acb7b64e2ea8dd895dd27616f5");
    }

    private static RequestSpecBuilder getDefaultHeaderForXMlApiPrices() {
        return new RequestSpecBuilder().addHeader("Accept", "application/vnd.who.Sportsbook+xml;v=1;charset=utf-8").
                addHeader("who-apiKey", "l7xxdbad8c0b183147f99f464f42ded229be");
    }
}
