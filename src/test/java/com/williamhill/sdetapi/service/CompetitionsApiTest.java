package com.williamhill.sdetapi.service;

import com.jayway.restassured.response.Response;
import com.williamhill.sdetapi.service.login.AuthenticateRequest;
import org.apache.http.HttpStatus;
import org.junit.Test;

public class CompetitionsApiTest extends BasicLoginTest {

    @Test
    public void verifyPricesOnBet() {
        Response loginResponse = new AuthenticateRequest().authenticatePrices();
        loginResponse.
                then().
                statusCode(HttpStatus. SC_OK);
        System.out.println(loginResponse.body().print());
    }
}
