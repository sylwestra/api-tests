package com.williamhill.sdetapi.service;

import com.jayway.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Test;
import com.williamhill.sdetapi.service.login.AuthenticateRequest;

public class BetTest extends BasicLoginTest {


    @Test
    public void verifyIfBetIsPlaced() {
        Response loginResponse = new AuthenticateRequest().authenticateBet(ticket.getMyTicket());

        System.out.println(loginResponse.body().print());
        loginResponse.
                then().
                statusCode(HttpStatus. SC_CREATED);
    }

    @Test
    public void verifyIfBetIsNotPlaced() {
        Response loginResponse = new AuthenticateRequest().authenticateBetWithoutReuiredParam(ticket.getMyTicket());

        System.out.println(loginResponse.body().print());
        Assert.assertEquals(loginResponse.path("whoBets.whoFaults.fault.children()[0]").toString(), "10070");
        loginResponse.
                then().
                statusCode(HttpStatus. SC_OK);
    }

    @Test
    public void verifyIfBetIsVisible() {
        Response loginResponse = new AuthenticateRequest().getBet(ticket.getMyTicket());

        loginResponse.
                then().
                statusCode(HttpStatus. SC_OK);

        System.out.println(loginResponse.body().print());
    }



}
