package com.williamhill.sdetapi.service;

import com.jayway.restassured.response.Response;
import com.williamhill.sdetapi.service.login.LoginRequest;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Test;

public class LoginTest {
    @Test
    public void verifyIfCustomerHasMissingUsernameParameter() {
        Response loginResponse = new LoginRequest().loginCustomerWithoutUsername();

        loginResponse.
                then().
                statusCode(HttpStatus.SC_OK);
        Assert.assertEquals(loginResponse.path("whoFaults.fault.children()[0]").toString(), "20010");
        Assert.assertEquals(loginResponse.path("whoFaults.fault.children()[1]").toString(), "Missing request parameter");
        Assert.assertEquals(loginResponse.path("whoFaults.fault.children()[2]").toString(), "username");
        System.out.println(loginResponse.body().print());

    }

    @Test
    public void verifyIfCustomerHasMissingPasswordParameter() {
        Response loginResponse = new LoginRequest().loginCustomerWithoutPassword();

        loginResponse.
                then().
                statusCode(HttpStatus.SC_OK);
        Assert.assertEquals(loginResponse.path("whoFaults.fault.children()[0]").toString(), "20010");
        Assert.assertEquals(loginResponse.path("whoFaults.fault.children()[1]").toString(), "Missing request parameter");
        Assert.assertEquals(loginResponse.path("whoFaults.fault.children()[2]").toString(), "password");
        System.out.println(loginResponse.body().print());

    }
}
