package com.williamhill.sdetapi.service;

import com.jayway.restassured.response.Response;
import com.williamhill.sdetapi.service.login.AuthenticateRequest;
import com.williamhill.sdetapi.service.login.LoginRequest;
import org.apache.http.HttpStatus;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BasicLoginTest {

    protected static Ticket ticket = new Ticket();

    @BeforeClass
    public static void setUp() {
        Response loginResponse = new AuthenticateRequest().loginCustomer();

        loginResponse.
                then().
                statusCode(HttpStatus.SC_CREATED);

        ticket.setMyTicket(loginResponse.path("whoSessions.children()[1]").toString());
    }

}
