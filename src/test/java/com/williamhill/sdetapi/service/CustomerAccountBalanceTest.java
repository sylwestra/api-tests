package com.williamhill.sdetapi.service;

import com.jayway.restassured.response.Response;
import com.williamhill.sdetapi.service.login.AuthenticateRequest;
import org.apache.http.HttpStatus;

import org.junit.Test;


public class CustomerAccountBalanceTest extends BasicLoginTest {

    @Test
    public void verifyCorrectBehaviourOfCustomerBalance() {

        Response loginResponse = new AuthenticateRequest().authenticateBalance(ticket.getMyTicket());

        loginResponse.
                then().
                statusCode(HttpStatus.SC_OK);
        System.out.println(loginResponse.body().print());
    }

}
